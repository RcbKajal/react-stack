"use client";

import React, { useState, useEffect } from "react";

const ShopListComponent = ({ shopData }: any) => {
  const [searchInput, setSearchInput] = useState("");
  const [selectedCategory, setSelectedCategory] = useState("");
  const [hoveredShop, setHoveredShop] = useState<any>(null);
  const [categories, setCategories] = useState<string[]>([]);

  useEffect(() => {
    // Extract unique categories from shopData
    const uniqueCategories:any = [...new Set(shopData.map((shop: any) => shop.category))];
    setCategories(uniqueCategories);
  }, [shopData]);

  const handleSearchInput = (event: any) => {
    setSearchInput(event.target.value);
  };

  const handleCategoryChange = (event: any) => {
    const newCategory = event.target.value;
    setSelectedCategory(newCategory);
  };

  const handleMouseEnter = (shop: any) => {
    setHoveredShop(shop);
  };

  const handleMouseLeave = () => {
    setHoveredShop(null);
  };

  const filteredShopData = shopData
    .filter((shop: any) => {
      const lowerCaseSearchInput = searchInput.toLowerCase();
      const lowerCaseCategory = selectedCategory.toLowerCase();

      return (
        (shop.name.toLowerCase().includes(lowerCaseSearchInput) ||
          shop.category.toLowerCase().includes(lowerCaseSearchInput)) &&
        (selectedCategory === "" || shop.category.toLowerCase() === lowerCaseCategory)
      );
    });

  return (
    <>
      <div style={{ display: "flex", alignItems: "center" }}>
        <div>
          <input
            style={{
              borderWidth: 2,
              borderColor: "#ddd",
              padding: 10,
              width: "100%",
            }}
            type="text"
            value={searchInput}
            onChange={handleSearchInput}
            placeholder="Search by name or category"
          />
        </div>
        <div>
          <select onChange={handleCategoryChange} value={selectedCategory}>
            <option value="">All</option>
            {categories.map((category) => (
              <option key={category} value={category}>
                {category}
              </option>
            ))}
          </select>
        </div>
      </div>

      <ul>
        {filteredShopData.map((shop: any) => (
          <li
            key={shop.name}
            onMouseEnter={() => handleMouseEnter(shop)}
            onMouseLeave={handleMouseLeave}
          >
            <img
              src={shop.picture}
              alt={shop.name}
              width="100"
              height="100"
            />
            {shop.name} - {shop.category}
          </li>
        ))}
      </ul>

      {hoveredShop && (
        <div
          className="shop-details-popup"
          style={{
            position: "absolute",
            top: "50%",
            left: "50%", // Adjust the position as needed
            background: "#fff",
            padding: "10px",
            border: "1px solid #ccc",
            boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2)",
            zIndex: "1",
          }}
        >
          <h2>{hoveredShop.name}</h2>
          <p>Category: {hoveredShop.category}</p>
          {/* Add more shop details here */}
        </div>
      )}
    </>
  );
};

export default ShopListComponent;



