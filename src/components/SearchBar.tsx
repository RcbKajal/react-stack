"use client";
import React, { useState } from 'react'

const SearchBar = () => {
    const [searchInput, setSearchInput] = useState('');
      const handleSearchInput = (event:any) => {
    setSearchInput(event.target.value);
  };
  return (
    <div>

           {/* <Select
        value={selectedCategory}
        onChange={handleCategoryChange}
        options={categories}
      /> */}
      <input
        type="text"
        value={searchInput}
        onChange={handleSearchInput}
        placeholder="Search by name or category"
      />
      {/* <button onClick={filterShopData}>Filter</button> */}
    </div>
  )
}

export default SearchBar