import React from 'react';
import ShopListComponent from './ShopListComponent';

const ShopPage = () => {
  const shopData = [
    {
      name: 'Shop 1',
      category: 'Clothing',
      priceRange: '$$',
      rating: 4.5,
      color: 'red',
      picture: '/white_house.png'
    },
    {
      name: 'Shop 2',
      category: 'Electronics',
      priceRange: '$',
      rating: 3.5,
      color: 'blue',
      picture: '/white_house.png'
    }
  ];

  return (
    <div>
      <h1>Shop List</h1>
      <ShopListComponent shopData={shopData} />
    </div>
  );
};

export default ShopPage;